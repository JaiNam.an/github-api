from django.urls import path
from .views import home_page, download_file

app_name = 'starred_projects'
urlpatterns = [
    path('', home_page, name='starred_home_page'),
    path('download/', download_file, name='download_file'),
]
