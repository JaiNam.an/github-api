from json import dumps
from environ import Env
from pathlib import Path
from os.path import join
from github import Github
from pandas import read_csv


class StarredProject:
    def __init__(self):
        self.__BASE_DIR = Path(__file__).resolve().parent.parent.parent
        self.__ENV = Env()
        self.__ENV.read_env(join(self.__BASE_DIR, '.env'))
        self.__GITHUB = Github(self.__ENV('GITHUB_TOKEN'))
        self.__JSONFILE = join(self.__BASE_DIR, 'github_project', 'json', 'starred.json')
        self.__CSVFILE = join(self.__BASE_DIR, 'github_project', 'csv', 'contributors.csv')

    # Method returns the top 10 contributors username.
    def __get_username_list(self):
        csv_data = read_csv(self.__CSVFILE)
        usernames = csv_data.loc[:, 'username'].tolist()
        return usernames

    # Method returns the dictionary of username with the list of their starred projects.
    def __get_starred_project_dict(self):
        usernames = self.__get_username_list()
        starred_project_dict = dict()

        for username in usernames:
            user = self.__GITHUB.get_user(username)
            # list of starred projects
            starred_repo_list = [starred_repo.full_name for starred_repo in user.get_starred()]
            # dictionary stores username as key and stores list of starred project as value.
            starred_project_dict[username] = starred_repo_list

        return starred_project_dict

    # Method dumps the starred project dictionary to json file.
    def dump_data_to_json(self):
        project_dict = self.__get_starred_project_dict()
        json_obj = dumps(project_dict, indent=4)
        with open(self.__JSONFILE, 'w') as jsonfile:
            jsonfile.write(json_obj)
