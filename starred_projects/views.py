from pathlib import Path
from os.path import join
from mimetypes import guess_type
from.bin.starred_project import StarredProject
from django.shortcuts import render, HttpResponse


# Create your views here.
BASE_DIR = Path(__file__).resolve().parent.parent
JSON_FILE = join(BASE_DIR, 'github_project', 'json', 'starred.json')


def home_page(request):
    starred_project = StarredProject()
    starred_project.dump_data_to_json()
    return render(request, 'starred_projects/index.html')


def download_file(request):
    filename = 'starred.json'
    file = open(JSON_FILE, 'r')
    mime_type, _ = guess_type(JSON_FILE)
    response = HttpResponse(file, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % filename

    return response
