from django.apps import AppConfig


class StarredProjectsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'starred_projects'
