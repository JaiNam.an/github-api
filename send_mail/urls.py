from django.urls import path
from .views import home_page, send_mail


app_name = 'mail'
urlpatterns = [
    path('', home_page, name='mail_home_page'),
    path('send/', send_mail, name='send_mail'),
]
