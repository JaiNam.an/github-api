from pathlib import Path
from os.path import join
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import render, HttpResponse


# Create your views here.
BASE_DIR = join(Path(__file__).resolve().parent.parent, 'github_project')

with open(join(BASE_DIR, 'json', 'info.json'), 'r') as info_json:
    INFO_JSON = info_json.read()

with open(join(BASE_DIR, 'json', 'starred.json'), 'r') as starred_json:
    STARRED_JSON = starred_json.read()

with open(join(BASE_DIR, 'csv', 'contributors.csv'), 'r') as contributors_csv:
    CONTRIBUTORS_CSV = contributors_csv.read()


def home_page(request):
    return render(request, 'send_mail/index.html')


def send_mail(request):
    if request.method == 'POST':
        try:
            # Required details to send mail.
            subject = 'Github Organisation details.'
            message = 'Your requested details of github organisation is enclosed with this mail.'
            sender = settings.EMAIL_HOST_USER
            receiver = request.POST['email']

            # Raising exception if receiver's mail is empty.
            if receiver == '':
                raise Exception('email')

            # Sending mail with required file as an attachment with the mail.
            mail = EmailMessage(subject, message, sender, [receiver])
            mail.attach('info.json', INFO_JSON, 'file/json')
            mail.attach('starred.json', STARRED_JSON, 'file/json')
            mail.attach('contributors.json', CONTRIBUTORS_CSV, 'file/csv')
            mail.send()
            # Context for templates if mail sent successfully.
            msg = 'Mail sent!'
            color = 'green'
            button = 'Send Again'

        # Handling exception if something goes wrong while sending email.
        except Exception as e:
            if 'email' in str(e):
                msg = 'Enter your email id!'
            else:
                msg = e
            # Context for templates if error occurs.
            color = 'red'
            button = 'Try Again'

        return render(request, 'send_mail/message.html', {'message': msg, 'color': color, 'button': button})

    return render(request, 'send_mail/index.html')
