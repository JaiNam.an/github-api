from django.shortcuts import HttpResponseRedirect


def redirect_to_url_validator(request):
    return HttpResponseRedirect('url/')
