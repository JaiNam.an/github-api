from re import match
from requests import head


class Validator:
    def __init__(self):
        self.req = None
        self.__github_url_regex = r'^(http(s?):\/\/)?(www\.)?github\.com\/([A-Za-z0-9\-]{1,})+\/?$'

    # Method checks if given url is valid or not on the basis of response code.
    def get_link_status_code(self, url):
        if not ('http' in url):
            url = 'https://' + url
        self.req = head(url, verify=False, timeout=5)
        if self.req.status_code == 200:
            return True
        return False

    def url_validator(self, url):
        if match(self.__github_url_regex, url) and self.get_link_status_code(url):
            return True
        return False
