from urllib.parse import urlparse
from .bin.validator import Validator
from django.shortcuts import render, HttpResponseRedirect


# Create your views here.
def dump_url_to_file(url: str):
    from pathlib import Path
    from os.path import join

    path = join(Path(__file__).resolve().parent.parent, 'github_project', 'bin', 'url.txt')
    # Filtering out organisation username from the url.
    if not ('http' in url):
        url = 'https://' + url

    organisation = urlparse(url).path[1:]
    # Writing url and organisation name to the file so any app can access it.
    with open(path, 'w') as file:
        file.writelines([url, '\n', organisation])


def home_page(request):
    if request.method == 'POST':
        validate = Validator()
        url = request.POST['url']
        if validate.url_validator(url):
            dump_url_to_file(url)
            return HttpResponseRedirect('../commits/')
        else:
            return render(request, 'url_validator/not_valid.html')

    return render(request, 'url_validator/index.html')
