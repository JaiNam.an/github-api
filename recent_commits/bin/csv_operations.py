import pandas
from json import dumps
from pathlib import Path
from os.path import join
from csv import DictReader


class CsvOperation:
    # Paths of required CSV file and JSON file are defined.
    def __init__(self):
        self.__file = Path(__file__).resolve().parent.parent.parent
        self.__json_file = join(self.__file, 'github_project', 'json', 'info.json')
        self.__csv_file = join(self.__file, 'github_project', 'csv', 'all_commits.csv')

    # This method sorts the data in respect of date of commit.
    def sort_csv(self):
        data = pandas.read_csv(self.__csv_file)
        sorted_csv = data.sort_values(by='date', ascending=False)
        sorted_csv.to_csv(self.__csv_file, index=False)

    # This method dumps the csv data into json file using csv DictReader.
    def csv_to_json(self):
        # Extracting top 500 commits from sorted csv file.
        with open(self.__csv_file, newline='') as csvfile:
            reader = DictReader(csvfile)
            csv_data = list()
            row_num = 500
            for row in reader:
                if row_num == 0:
                    break
                csv_data.append(row)
                row_num -= 1

        # Dumping json file from list of commits data.
        json_obj = dumps(csv_data, indent=4)
        with open(self.__json_file, "w") as jsonfile:
            jsonfile.write(json_obj)
