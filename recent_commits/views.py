from pathlib import Path
from os.path import join
from mimetypes import guess_type
from .bin.csv_operations import CsvOperation
from .bin.github_organisation import GithubOrganisation
from django.shortcuts import render, HttpResponse, HttpResponseRedirect


# Create your views here.
def home_page(request):
    csv_op = CsvOperation()
    github = GithubOrganisation()
    github.dump_all_commits_in_csv()
    csv_op.sort_csv()
    csv_op.csv_to_json()
    return render(request, "recent_commits/index.html")


def download_file(request):
    filepath = Path(__file__).resolve().parent.parent
    filepath = join(filepath, 'github_project/json/info.json')
    filename = 'info.json'

    file = open(filepath, 'r')
    mime_type, _ = guess_type(filepath)
    response = HttpResponse(file, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % filename

    return response
